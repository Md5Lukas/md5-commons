package de.md5lukas.commons.tuples;

public class Tuple2<L, R> {

	private final L l;
	private final R r;

	private Tuple2(L l, R r) {
		this.l = l;
		this.r = r;
	}

	public L getL() {
		return l;
	}

	public R getR() {
		return r;
	}

	public static <L, R> Tuple2<L, R> of(L l, R r) {
		return new Tuple2<>(l, r);
	}
}
